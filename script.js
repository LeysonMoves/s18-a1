let trainer = {
	name: "Ash Ketchum",
	talk: function(){
		console.log('Pikachu! I choose you!');
		},
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty'],
	},
}
console.log(trainer);

// NUMBER 5
console.log('Result of dot notation:');
console.log(trainer.name);
console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);

// NUMBER 6
console.log("Result of talk method");
trainer.talk();

// Number 7
function Pokemon(name, level){
		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		this.tackle = function(target){
			console.log(this.name + ' tackled ' + target.name);
			target.health = target.health - this.attack;
			console.log(target.name + "'s" + " health is now reduced to "+  target.health);
			if (target.health <= 0){
				this.faint(target)

			}
		     console.log(target);
		}
		this.faint = function(target){
			console.log(target.name + " fainted.");
		  	

		}



}
let pikachu = new Pokemon ("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon ("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon ("Mewtwo", 100);
console.log(mewtwo);

// Number 9
pikachu.tackle(geodude);


